defmodule GitlabSampleTest do
  use ExUnit.Case
  doctest GitlabSample

  test "add two values" do
    assert GitlabSample.add(3, 5) == 8
  end

  test "multiply two values" do
    assert GitlabSample.multiply(3, 5) == 15
  end

  test "subtract value" do
    assert GitlabSample.subtract(3, 5) == -2
  end
end
